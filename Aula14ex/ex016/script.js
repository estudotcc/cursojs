function contar() {
    var inicio = Number(document.getElementById('inicio').value)
    var fim = Number(document.getElementById('fim').value)
    var passo = Number(document.getElementById('passo').value)
    var conta = document.querySelector('div#conta')
    conta.innerHTML = ''
    if (passo == 0) {
        alert('Passo inválido. Será considerado passo = 1')
        passo = 1
    }
    if (inicio == 0 || fim == 0 || passo == 0) {
        conta.innerHTML = 'Impossível contar'
    } else if(inicio < fim){//contagem crescente
        for (inicio; inicio <= fim; inicio += passo) {
            conta.innerText += inicio + ' \u{1F449}'
        }
        conta.innerText += '\u{1F3C1}'
    }else if(inicio > fim){ //Contagem regresiva
        for (inicio; inicio >= fim; inicio -= passo) {
            conta.innerText += inicio + ' \u{1F449}'
        }
        conta.innerText += '\u{1F3C1}'
    }
}
