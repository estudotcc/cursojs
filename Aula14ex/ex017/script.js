function calcular() {
    var numero = Number(document.getElementById('numero').value)
    var resultado = document.getElementById('res')
    resultado.innerHTML = ''
    if (numero != 0) {
        for (let index = 1; index <= 10; index++) {
            let item = document.createElement('option')
            item.innerText += `${numero} x ${index} = ${numero * index}`
            item.value = `tab${index}`
            resultado.appendChild(item)
        }
    }else{
        alert('Favor digite um número')
        resultado.innerHTML = '<p>Digite um número acima!</p>'
    }
}
