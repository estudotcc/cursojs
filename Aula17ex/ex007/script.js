let number = document.getElementById('numero')
let res = document.getElementById('res')
var select = document.getElementById('valores')
var valores = []

function adicionar() {
    if (Number(number.value) == 0 || estaNaLista(number.value, valores)) {
        alert('valor inválido ou já encontrado')
    } else if (Number(number.value) > 100 || Number(number.value) < 1) {
        alert('valor não está entre 1 a 100')
    } else {
        let item = document.createElement('option')
        item.innerHTML += `valor ${Number(number.value)} adicionado`
        select.appendChild(item)
        valores.push(Number(number.value))
        res.innerHTML = ''
    }
    number.value = ''
    number.focus() //coloca o cursor no number
}

function finalizar() {
    if (valores.length == 0) {
        alert('Adicione valores antes de finalizar')
    } else {
        // res.innerHTML = ''

        res.innerHTML =
            `<p>Ao todo, temos ${valores.length} números cadastrados</p>
            <p>O maior valor informado foi ${maiorNumero()}</p>
            <p>O menor valor informado foi ${menorNumero()}</p>
            <p>Somando todos os valores dá ${somaNumeros()}</p>
            <p>A média é igual a ${mediaNumeros()}</p>`
    }
}

function estaNaLista(n, l) {
    if (l.indexOf(Number(n)) != -1) { //-1 indica que o valor não foi encontrado na list
        return true
    } else {
        return false
    }
}

function maiorNumero() {
    var maior = 0 
    for (let key in valores) {
        if (valores[key] > maior) {
            maior = valores[key]
        }
    }
    return maior
}

function menorNumero() {
    var menor = 101 
    for (let key in valores) {
        if (valores[key] < menor) {
            menor = valores[key]
        }
    }
    return menor
}

function mediaNumeros() {
    return somaNumeros()/valores.length
}

function somaNumeros(){
    var soma = 0
    for (const key in valores) {
        soma += valores[key]
    }
    return soma
}
