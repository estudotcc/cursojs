function soma(n1=0, n2=0){ //caso n1 e n2 não tenham valores nas chamadas, serão inicializadas com os parâmetros pré-definidos
    return n1 + n2
}

console.log(soma())

//////////////////////////////////

let v = function(x){ //JS é uma linguagem funcional, por fazer isso
    return x*2
}

console.log(v(5))

/////////////////////////////// FUNÇÃO RECURSIVA (poderia ser feito com for)
function fatorial(n) {
    if (n == 1) {
        return 1
    } else {
        return n * fatorial(n-1)
    }
}