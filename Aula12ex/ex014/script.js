function carregar(){
    var msg = window.document.getElementById('msg')
    var img = window.document.querySelector('img#imagem')
    var data = new Date()
    var hora = data.getHours()
    msg.innerHTML = `Agora são ${hora} horas`
    if(hora >= 0 && hora < 12){
        img.src = 'foto-dia.png' //bom dia
        window.document.body.style.background = 'yellow'
    }else if (hora >= 12 && hora <18){
        img.src = 'foto-tarde.png'//boa tarde       
        window.document.body.style.background = 'orange'
    }else{
        img.src = 'foto-noite.png'//boa noite
        window.document.body.style.background = 'grey'
    }
}
